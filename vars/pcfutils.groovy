void call(){
    echo "please call specific function"
}

def deploy(Map config){
    echo "got deploy call"
    sh "cf login -a ${config.dev.pcf_url} -o ${config.org} -u 'siva.docker@gmail.com' -p 'Devops@2018'"
    def manifest = [:]
    manifest.applications = []
    def appManifest = [:]
    appManifest = config.dev.applications.first()
    echo "manifest is: ${appManifest}"
    appManifest.put('path',"${config.app_fileName}")
    manifest.applications.add(appManifest)
    writeYaml file: "manifest.yml", data: manifest
    sh "cat manifest.yml"
    sh "cf push -f manifest.yml"
}

def tearDown(Map config){
    echo "got teardown call"
    sh "cf create-app-manifest ${config.dev.applications.first().name} -p app.yaml"
    sh "cf delete -f -r ${config.dev.applications.first().name}"
}