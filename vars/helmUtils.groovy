void call(){
    echo "please call specific function"
}

def deploy(){
    if(fileExists('helm')){
        sh "gcloud auth activate-service-account --key-file=/home/jenkins/creds.json"
        sh "gcloud container clusters get-credentials cluster-1 --zone us-central1-c --project carbon-storm-273503"
        def status = sh(script: 'helm history hello', returnStatus: true)
        if(status !=0 ){
            echo "First time deployment"
            sh "helm install helm/hello-spring/ --name-template hello"
        }else{
            echo "upgrading deployment"
            sh "helm upgrade hello helm/hello-spring"
        }
    }
}