void call(){
    echo "please call specific function"
}

def deploy(){
    if(fileExists('kubernetes')){
        sh "pwd && ls -ltr"
        sh "gcloud auth activate-service-account --key-file=/home/jenkins/creds.json"
        sh "gcloud container clusters get-credentials cluster-1 --zone us-central1-c --project carbon-storm-273503"
        sh "kubectl get nodes"
        //sh "kubectl apply -f deployment.yaml"
        sh(script: 'kubectl apply -f kubernetes/deployment.yaml', returnStatus: true)
        def status = sh(script: 'kubectl rollout status -w deployment dep-test', returnStatus: true)
        if(status != 0){
            sh "kubectl rollout undo deployment dep-test"
        }
    }
}
